//
//  Plugin.m
//  XCodePlugin
//
//  Created by Dash on 7/05/13.
//  Copyright (c) 2013 Dash. All rights reserved.
//

#import "Plugin.h"
#import "MacProxyPlugin-Prefix.pch"

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <WebKit/WebKit.h>
#import <string.h>
#import "ASIHTTPRequest.h"



@implementation Plugin

const char* createEmptyString()
{
    char* ret = malloc(1);
    ret[0] = '\0';
    return ret;
}

const char * getProxy(){
    @autoreleasepool {
        @try{
            NSString* final_proxy = nil;
            CFDictionaryRef proxies = SCDynamicStoreCopyProxies(NULL);
            NSDictionary * proxyConfiguration = [( NSDictionary*) proxies objectForKey:@"__SCOPED__"];
    ;
            id key = nil;
            if([proxyConfiguration allKeys]){
                key = [[proxyConfiguration allKeys] objectAtIndex:0];
            }
            if([proxyConfiguration objectForKey:key]){
                NSString* httpproxy = [[proxyConfiguration objectForKey:key] objectForKey:@"HTTPProxy"];
                NSString* httpport = [[proxyConfiguration objectForKey:key] objectForKey:@"HTTPPort"];
                
                if(httpproxy){
                   final_proxy = [NSString stringWithFormat:@"%@:%@", httpproxy, httpport];
                }
                else if([[proxyConfiguration objectForKey:key] objectForKey:@"ProxyAutoConfigURLString"]){
                    NSString* proxy_url = [[proxyConfiguration objectForKey:key] objectForKey:@"ProxyAutoConfigURLString"];
                    NSLog(@"autoconfig url: %@", proxy_url);
      
                    NSURL *url = [NSURL URLWithString:@"http://google.com"];
                    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                    [request startSynchronous];
                    
                    final_proxy = [NSString stringWithFormat:@"%@:%d",request.proxyHost, request.proxyPort];
                }
                
            }
            
            if(final_proxy != nil){
                const char * proxy = [final_proxy UTF8String];
                NSLog(@"[MacProxyPlugin] - Proxy (just before return): %@",final_proxy);
                char* res = (char*)malloc(strlen(proxy) + 1);
                strcpy(res, proxy);
                return res;
            }else {
                return createEmptyString();
            }
        }@catch (NSException * e) {
            NSLog(@"\nException [MacProxyPlugin] - %@\n",e);
            return nil;
        }
     }
}




@end
